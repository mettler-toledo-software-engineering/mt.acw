﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace ACWRecorder_VS12.Infrastructure
{
    public class Settings
    {
        /// <summary>
        /// Gets or sets number of the COMPort of the scale.
        /// </summary>
        public string Com_Scale { get; set; }

        /// <summary>
        /// Gets or sets number of the COMPort of the scanner
        /// </summary>
        public string Com_Scanner { get; set; }

        /// <summary>
        /// Gets or sets the name of the attached printer
        /// </summary>
        public string PrinterName { get; set; }

        /// <summary>
        /// Gets or sets the data path of the application
        /// </summary>
        public string FolderPath { get; set; }

        /// <summary>
        /// Gets or sets the extension of the file created
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// Gets or sets the flag if a acknowledge shoud be played
        /// </summary>
        public bool PlaySound { get; set; }

        /// <summary>
        /// Gets or sets the flag if a rest of a code should be discarded or written
        /// </summary>
        public bool DismissRest { get; set; }

        /// <summary>
        /// Gets or sets the length of the first field of a code
        /// </summary>
        public int Field1Length { get; set; }

        /// <summary>
        ///  Gets or sets the length of the second field of a code
        /// </summary>
        public int Field2Length { get; set; }

        /// <summary>
        ///  Gets or sets the length of the thrid field of a code
        /// </summary>
        public int Field3Length { get; set; }

        private static Settings current;

        private static readonly XmlSerializer serializer = new XmlSerializer(typeof(Settings));
        private static readonly string fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "MTACWRecorder", "ACWRecorder.xml");

        private static Settings Load()
        {
            if (File.Exists(fileName))
            {
                using (var fs = File.Open(fileName, FileMode.Open))
                {
                    try
                    {
                        return (Settings)serializer.Deserialize(fs);
                    }
                    catch (Exception)
                    {
                        return new Settings();
                    }

                }
            }
            else
                return new Settings();

        }

        public void Save()
        {
            if (!Directory.Exists(Path.GetDirectoryName(fileName)))
                Directory.CreateDirectory(Path.GetDirectoryName(fileName));

            using (var fs = File.Open(fileName, FileMode.Create))
            {
                serializer.Serialize(fs, this);
            }
        }

        public static Settings Current
        {
            get
            {
                if (current == null)
                {
                    current = Load();
                }
                return current;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Settings"/> class.
        /// </summary>
        public Settings()
        {
            Com_Scale = "Keine";
            Com_Scanner = "Keine";
            PrinterName = "Keiner";
            Extension = ".CSV";
            PlaySound = true;
            Field1Length = 4;
            Field2Length = 4;
            Field3Length = 0;
            DismissRest = false;

            FolderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
        }
    }
}
