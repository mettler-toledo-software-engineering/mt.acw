﻿using MT.Foundation.Interfaces.Communication;
using System;
using System.IO.Ports;

namespace ACWRecorder_VS12.Infrastructure
{
    internal class SystemPortsSerialPort : ITransportStream
    {
        private readonly SerialPort serialPort;
        private bool isOpen;

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemPortsSerialPort"/> class.
        /// </summary>
        public SystemPortsSerialPort(SerialPort serialPort)
        {
            this.serialPort = serialPort;
        }

        public event EventHandler DataReceived;

        public int Read(byte[] data, int offset, int count)
        {
            return serialPort.Read(data, offset, count);
        }

        public void Write(byte[] data, int offset, int count)
        {
            serialPort.Write(data, offset, count);
        }

        public void Close()
        {
            if (!isOpen)
                return;

            isOpen = false;
            serialPort.DataReceived -= OnDataReceived;
            serialPort.Close();

            if (Closed != null)
                Closed(this, EventArgs.Empty);
        }

        public event EventHandler Closed;

        public bool IsOpen
        {
            get { return isOpen; }
        }

        public void Open()
        {
            if (isOpen)
                return;

            serialPort.Open();
            serialPort.DataReceived += OnDataReceived;

            isOpen = true;
            try
            {
                if (Opened != null)
                    Opened(this, EventArgs.Empty);
            }
            catch
            {
                isOpen = false;
                throw;
            }
        }

        void OnDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (DataReceived != null)
                DataReceived(this, EventArgs.Empty);
        }

        public event EventHandler Opened;
    }
}
