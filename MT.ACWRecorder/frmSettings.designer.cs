﻿namespace MT.ACWRecorder
{
    partial class frmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSettings));
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.comboInstalledComsScale = new Telerik.WinControls.UI.RadDropDownList();
            this.comboInstalledComsScanner = new Telerik.WinControls.UI.RadDropDownList();
            this.fbdReports = new System.Windows.Forms.FolderBrowserDialog();
            this.cmdChooseReportFolder = new Telerik.WinControls.UI.RadButton();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.comboInstalledPrinters = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.cmdSave = new Telerik.WinControls.UI.RadButton();
            this.lblFolder = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.txtExtension = new Telerik.WinControls.UI.RadTextBox();
            this.cbPlaySound = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.cbDismissRest = new Telerik.WinControls.UI.RadCheckBox();
            this.spField1 = new Telerik.WinControls.UI.RadSpinEditor();
            this.spField2 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.spField3 = new Telerik.WinControls.UI.RadSpinEditor();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboInstalledComsScale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboInstalledComsScanner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdChooseReportFolder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboInstalledPrinters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtension)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPlaySound)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDismissRest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spField1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spField2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spField3)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Underline);
            this.radLabel1.Location = new System.Drawing.Point(27, 112);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(154, 25);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Waagenschnittstelle";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Underline);
            this.radLabel2.Location = new System.Drawing.Point(27, 188);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(152, 25);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "Scannerschnittstelle";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Underline);
            this.radLabel3.Location = new System.Drawing.Point(27, 12);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(126, 25);
            this.radLabel3.TabIndex = 2;
            this.radLabel3.Text = "Dateiverzeichnis";
            // 
            // comboInstalledComsScale
            // 
            this.comboInstalledComsScale.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.comboInstalledComsScale.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.comboInstalledComsScale.Location = new System.Drawing.Point(27, 143);
            this.comboInstalledComsScale.Name = "comboInstalledComsScale";
            this.comboInstalledComsScale.Size = new System.Drawing.Size(154, 27);
            this.comboInstalledComsScale.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Descending;
            this.comboInstalledComsScale.TabIndex = 0;
            this.comboInstalledComsScale.Text = "Keine";
            this.comboInstalledComsScale.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.comboInstalledComsScale_SelectedIndexChanged);
            // 
            // comboInstalledComsScanner
            // 
            this.comboInstalledComsScanner.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.comboInstalledComsScanner.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.comboInstalledComsScanner.Location = new System.Drawing.Point(27, 219);
            this.comboInstalledComsScanner.Name = "comboInstalledComsScanner";
            this.comboInstalledComsScanner.Size = new System.Drawing.Size(154, 27);
            this.comboInstalledComsScanner.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Descending;
            this.comboInstalledComsScanner.TabIndex = 2;
            this.comboInstalledComsScanner.Text = "Keine";
            this.comboInstalledComsScanner.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.comboInstalledComsScanner_SelectedIndexChanged);
            // 
            // cmdChooseReportFolder
            // 
            this.cmdChooseReportFolder.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdChooseReportFolder.Location = new System.Drawing.Point(27, 79);
            this.cmdChooseReportFolder.Name = "cmdChooseReportFolder";
            this.cmdChooseReportFolder.Size = new System.Drawing.Size(110, 27);
            this.cmdChooseReportFolder.TabIndex = 3;
            this.cmdChooseReportFolder.Text = "&Ändern";
            this.cmdChooseReportFolder.Click += new System.EventHandler(this.cmdChooseReportFolder_Click);
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Underline);
            this.radLabel4.Location = new System.Drawing.Point(207, 112);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(65, 25);
            this.radLabel4.TabIndex = 6;
            this.radLabel4.Text = "Drucker";
            // 
            // comboInstalledPrinters
            // 
            this.comboInstalledPrinters.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.comboInstalledPrinters.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.comboInstalledPrinters.Location = new System.Drawing.Point(207, 143);
            this.comboInstalledPrinters.Name = "comboInstalledPrinters";
            this.comboInstalledPrinters.Size = new System.Drawing.Size(154, 27);
            this.comboInstalledPrinters.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Descending;
            this.comboInstalledPrinters.TabIndex = 1;
            this.comboInstalledPrinters.Text = "Keiner";
            this.comboInstalledPrinters.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.comboInstalledPrinters_SelectedIndexChanged);
            // 
            // radButton1
            // 
            this.radButton1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.radButton1.Location = new System.Drawing.Point(393, 393);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(110, 27);
            this.radButton1.TabIndex = 5;
            this.radButton1.Text = "A&bbrechen";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdSave.ForeColor = System.Drawing.Color.Gray;
            this.cmdSave.Location = new System.Drawing.Point(260, 393);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(110, 27);
            this.cmdSave.TabIndex = 4;
            this.cmdSave.Text = "&Speichern";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // lblFolder
            // 
            this.lblFolder.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lblFolder.Location = new System.Drawing.Point(27, 43);
            this.lblFolder.Name = "lblFolder";
            this.lblFolder.Size = new System.Drawing.Size(37, 25);
            this.lblFolder.TabIndex = 10;
            this.lblFolder.Text = "N/A";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Underline);
            this.radLabel5.Location = new System.Drawing.Point(207, 188);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(255, 25);
            this.radLabel5.TabIndex = 11;
            this.radLabel5.Text = "Standard-Dateinamenerweiterung";
            // 
            // txtExtension
            // 
            this.txtExtension.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtExtension.Location = new System.Drawing.Point(207, 219);
            this.txtExtension.Name = "txtExtension";
            this.txtExtension.Size = new System.Drawing.Size(142, 27);
            this.txtExtension.TabIndex = 12;
            this.txtExtension.Text = ".CSV";
            // 
            // cbPlaySound
            // 
            this.cbPlaySound.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cbPlaySound.Location = new System.Drawing.Point(27, 371);
            this.cbPlaySound.Name = "cbPlaySound";
            this.cbPlaySound.Size = new System.Drawing.Size(168, 25);
            this.cbPlaySound.TabIndex = 13;
            this.cbPlaySound.Text = "mit Bestätigungston";
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Underline);
            this.radLabel6.Location = new System.Drawing.Point(27, 262);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(260, 25);
            this.radLabel6.TabIndex = 14;
            this.radLabel6.Text = "Eingabe/Barcode in Felder trennen";
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.radLabel7.Location = new System.Drawing.Point(27, 293);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(92, 25);
            this.radLabel7.TabIndex = 15;
            this.radLabel7.Text = "Feldlänge 1";
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.radLabel8.Location = new System.Drawing.Point(148, 293);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(92, 25);
            this.radLabel8.TabIndex = 16;
            this.radLabel8.Text = "Feldlänge 2";
            // 
            // cbDismissRest
            // 
            this.cbDismissRest.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cbDismissRest.Location = new System.Drawing.Point(384, 324);
            this.cbDismissRest.Name = "cbDismissRest";
            this.cbDismissRest.Size = new System.Drawing.Size(129, 25);
            this.cbDismissRest.TabIndex = 17;
            this.cbDismissRest.Text = "Rest verwerfen";
            // 
            // spField1
            // 
            this.spField1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.spField1.Location = new System.Drawing.Point(27, 324);
            this.spField1.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.spField1.Name = "spField1";
            this.spField1.Size = new System.Drawing.Size(78, 27);
            this.spField1.TabIndex = 18;
            this.spField1.TabStop = false;
            // 
            // spField2
            // 
            this.spField2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.spField2.Location = new System.Drawing.Point(148, 324);
            this.spField2.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.spField2.Name = "spField2";
            this.spField2.Size = new System.Drawing.Size(78, 27);
            this.spField2.TabIndex = 19;
            this.spField2.TabStop = false;
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.radLabel9.Location = new System.Drawing.Point(269, 293);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(92, 25);
            this.radLabel9.TabIndex = 20;
            this.radLabel9.Text = "Feldlänge 3";
            // 
            // spField3
            // 
            this.spField3.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.spField3.Location = new System.Drawing.Point(269, 324);
            this.spField3.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.spField3.Name = "spField3";
            this.spField3.Size = new System.Drawing.Size(78, 27);
            this.spField3.TabIndex = 21;
            this.spField3.TabStop = false;
            // 
            // frmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 432);
            this.Controls.Add(this.spField3);
            this.Controls.Add(this.radLabel9);
            this.Controls.Add(this.spField2);
            this.Controls.Add(this.spField1);
            this.Controls.Add(this.cbDismissRest);
            this.Controls.Add(this.radLabel8);
            this.Controls.Add(this.radLabel7);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.cbPlaySound);
            this.Controls.Add(this.txtExtension);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.lblFolder);
            this.Controls.Add(this.cmdSave);
            this.Controls.Add(this.radButton1);
            this.Controls.Add(this.comboInstalledPrinters);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.cmdChooseReportFolder);
            this.Controls.Add(this.comboInstalledComsScanner);
            this.Controls.Add(this.comboInstalledComsScale);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radLabel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSettings";
            this.Text = "Einstellungen";
            this.Load += new System.EventHandler(this.frmSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboInstalledComsScale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboInstalledComsScanner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdChooseReportFolder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboInstalledPrinters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtension)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPlaySound)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDismissRest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spField1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spField2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spField3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadDropDownList comboInstalledComsScale;
        private Telerik.WinControls.UI.RadDropDownList comboInstalledComsScanner;
        private System.Windows.Forms.FolderBrowserDialog fbdReports;
        private Telerik.WinControls.UI.RadButton cmdChooseReportFolder;
        private Telerik.WinControls.UI.RadDropDownList comboInstalledPrinters;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadButton cmdSave;
        private Telerik.WinControls.UI.RadLabel lblFolder;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBox txtExtension;
        private Telerik.WinControls.UI.RadCheckBox cbPlaySound;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadCheckBox cbDismissRest;
        private Telerik.WinControls.UI.RadSpinEditor spField1;
        private Telerik.WinControls.UI.RadSpinEditor spField2;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadSpinEditor spField3;

    }
}

