﻿using System;
using System.IO.Ports;
using System.Linq;
using System.Windows.Forms;
using MT.AcwRecorder.Infrastructure;
using System.Drawing.Printing;
using System.Reflection;
using Telerik.WinControls.UI;

namespace MT.ACWRecorder
{
    public partial class frmSettings : Form
    {
        private Settings config;

        public frmSettings(Settings config)
        {
            InitializeComponent();
            this.config = config;
            this.Text = "Einstellungen Version: " + Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }


        private void frmSettings_Load(object sender, EventArgs e)
        {
            PopulateInstalledPrinters();
            PopulateInstalledComs();
            lblFolder.Text = config.FolderPath;
            txtExtension.Text = config.Extension;
            cbPlaySound.Checked = config.PlaySound;
            spField1.Value = config.Field1Length;
            spField2.Value = config.Field2Length;
            spField3.Value = config.Field3Length;
            cbDismissRest.Checked = config.DismissRest;

        }


        private void PopulateInstalledComs()
        {
            comboInstalledComsScale.VisualListItemFormatting += new VisualListItemFormattingEventHandler(comboInstalledComs_VisualListItemFormatting);
            comboInstalledComsScanner.VisualListItemFormatting += new VisualListItemFormattingEventHandler(comboInstalledComs_VisualListItemFormatting);
            // Add list of installed com ports found to the combo box. 
            string[] ports = SerialPort.GetPortNames();

            comboInstalledComsScale.Items.Add("Keine");
            comboInstalledComsScanner.Items.Add("Keine");
            for (int i = 0; i < ports.Length; i++)
            {
                comboInstalledComsScale.Items.Add(ports[i]);
                comboInstalledComsScanner.Items.Add(ports[i]);
            }

            comboInstalledComsScale.SelectedIndex = comboInstalledComsScale.FindString(config.Com_Scale);
            comboInstalledComsScanner.SelectedIndex = comboInstalledComsScanner.FindString(config.Com_Scanner);

        }

        void comboInstalledComs_VisualListItemFormatting(object sender, VisualItemFormattingEventArgs args)
        {
            args.VisualItem.Font = comboInstalledComsScale.Font;
        }



        private void PopulateInstalledPrinters()
        {
            // Add list of installed printers found to the combo box. 
            // The pkInstalledPrinters string will be used to provide the display string.
            String pkInstalledPrinters;
            comboInstalledPrinters.Items.Add("Keiner");
            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                pkInstalledPrinters = PrinterSettings.InstalledPrinters[i];
                comboInstalledPrinters.Items.Add(pkInstalledPrinters);
            }

            comboInstalledPrinters.SelectedIndex = comboInstalledPrinters.FindString(config.PrinterName);

        }

        private void comboInstalledComsScale_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                // Set the COM in the combo box when the selection changes. 
                if (comboInstalledComsScale.SelectedIndex != -1)
                {
                    // The combo box's Text property returns the selected item's text, which is the COM Port name.
                    if (comboInstalledComsScale.Text != "Keine" && comboInstalledComsScale.Text == config.Com_Scanner)
                    {
                        MessageBox.Show("Die gleiche Schnittstelle wie der Scanner kann nicht verwendet werden", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        comboInstalledComsScale.SelectedIndex = comboInstalledComsScale.FindString("Keine");

                    }
                    config.Com_Scale = comboInstalledComsScale.Text;
                }
            }));
        }

        private void comboInstalledComsScanner_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            BeginInvoke(new Action(() =>
             {
                 // Set the COM in the combo box when the selection changes. 
                 if (comboInstalledComsScanner.SelectedIndex != -1)
                 {
                     // The combo box's Text property returns the selected item's text, which is the COM Port name.
                     if (comboInstalledComsScale.Text != "COM0" && comboInstalledComsScanner.Text == config.Com_Scale)
                     {
                         MessageBox.Show("Die gleiche Schnittstelle wie die Waage kann nicht verwendet werden", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                         comboInstalledComsScanner.SelectedIndex = comboInstalledComsScanner.FindString("Keine");
                     }
                     config.Com_Scanner = comboInstalledComsScanner.Text;
                 }
             }));
        }

        private void comboInstalledPrinters_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            // Set the printer to a printer in the combo box when the selection changes. 
            if (comboInstalledPrinters.SelectedIndex != -1)
            {
                // The combo box's Text property returns the selected item's text, which is the printer name.
                config.PrinterName = comboInstalledPrinters.Text;
            }
        }
        private void cmdChooseReportFolder_Click(object sender, EventArgs e)
        {
            fbdReports.SelectedPath = config.FolderPath;
            fbdReports.Description = "Ablageverzeichnis für die Protokolldateien wählen";
            if (fbdReports.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                config.FolderPath = fbdReports.SelectedPath;
                lblFolder.Text = config.FolderPath;
            }
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {

            if (!txtExtension.Text.StartsWith("."))
                config.Extension = ".";
            else
                config.Extension = "";
            config.Extension += txtExtension.Text;
            config.PlaySound = cbPlaySound.Checked;
            config.Field1Length = Convert.ToInt32(spField1.Value);
            config.Field2Length = Convert.ToInt32(spField2.Value);
            config.Field3Length = Convert.ToInt32(spField3.Value);
            config.DismissRest = cbDismissRest.Checked;
            config.Save();
            Close();
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            Close();
        }


    }
}
