﻿using System;
using System.Drawing;
using System.Windows.Forms;

using System.Linq;
using System.IO.Ports;
using MT.AcwRecorder.Infrastructure;
using MT.Foundation.Protocols.Sics.Client;
using MT.Foundation.Communication;
using MT.ACWRecorder.Infrastructure;
using System.IO;
using System.Drawing.Printing;
using System.Text;
using MT.Foundation.Interfaces.Communication;
using System.Media;


namespace MT.ACWRecorder
{

    /// <summary>
    /// Framework for running application as a tray app.
    /// </summary>
    /// <remarks>
    /// Tray app code adapted from "Creating Applications with NotifyIcon in Windows Forms", Jessica Fosler,
    /// http://windowsclient.net/articles/notifyiconapplications.aspx
    /// </remarks>
    public class CustomApplicationContext : ApplicationContext
    {

        public CommunicationEngine CommunicationEngine { get; private set; }

        private System.ComponentModel.IContainer components;	// a list of components to dispose when the context is disposed
        private NotifyIcon notifyIcon;
        private frmSettings settingsForm;
        private SerialPort scannerPort = null;
        private SicsClient scale = null;
        private Settings settings;
        private string fileName = "";
        private bool running = false;



        /// <summary>
        /// This class should be created and passed into Application.Run( ... )
        /// </summary>
        public CustomApplicationContext()
        {
            components = new System.ComponentModel.Container();
            CommunicationEngine = new CommunicationEngine();
            settings = Settings.Current;


            notifyIcon = new NotifyIcon(components)
            {
                ContextMenuStrip = new ContextMenuStrip(),
                Icon = Properties.Resources.Data_transmission,
                Text = "MT.ACWBalRecorder",
                Visible = true,
                BalloonTipText = "Speichert Wägedaten einer angeschlossenen Waage in eine CSV-Datei",
                BalloonTipTitle = "ACWRecorder-BalRecorder"

            };

            notifyIcon.ContextMenuStrip.Items.Add(new ToolStripMenuItem("Start", null, handleRecorder_Click, "Action"));
            notifyIcon.ContextMenuStrip.Items.Add(new ToolStripSeparator());
            notifyIcon.ContextMenuStrip.Items.Add(new ToolStripMenuItem("&Manuelle Einagbe", null, notifyIcon_DoubleClick, "Manuell"));
            notifyIcon.ContextMenuStrip.Items.Add(new ToolStripSeparator());
            notifyIcon.ContextMenuStrip.Items.Add(new ToolStripMenuItem("&Konfiguration", null, showfrmSettings_Click, "Config"));
            notifyIcon.ContextMenuStrip.Items.Add(new ToolStripMenuItem("&Ende", null, exitItem_Click, "End"));

            notifyIcon.ContextMenuStrip.Items["Manuell"].Enabled = false;

            notifyIcon.DoubleClick += notifyIcon_DoubleClick;

            OpenPorts();


        }

        private void showfrmSettings_Click(object sender, EventArgs e)
        {
            ShowSettingsForm();
        }

        private void handleRecorder_Click(object sender, EventArgs e)
        {
            HandleRecording();
        }

        private void OpenPorts()
        {
            string scalePort = SerialPort.GetPortNames().FirstOrDefault(d => d == settings.Com_Scale);
            ITransportStream systemscalePort;

            if (scalePort != null)
            {
                try
                {
                    systemscalePort = new SystemPortsSerialPort(new SerialPort(settings.Com_Scale, 9600, Parity.None, 8, StopBits.One));
                    scale = new SicsClient(CommunicationEngine, systemscalePort);
                    scale.TareTrackingMode = TareTrackingMode.Full;
                    scale.Open();
                }
                catch
                {

                }
            }
            string ScannerPort = SerialPort.GetPortNames().FirstOrDefault(d => d == settings.Com_Scanner);

            if (ScannerPort != null)
            {
                try
                {
                    scannerPort = new SerialPort("COM0", 9600, Parity.None, 8, StopBits.One);
                    scannerPort.PortName = settings.Com_Scanner;
                    scannerPort.DataReceived += new SerialDataReceivedEventHandler(scannerPort_DataReceived);
                    scannerPort.Open();
                }
                catch
                {

                }
            }
        }

        void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            if (running == false)
                MessageBox.Show("Aufzeichnung zuerst starten!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else if (scale == null)
                MessageBox.Show("Waage gibt keine Antwort !", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);

            else
            {
                string value = "000000000000";
                if (DataEntry.InputBox("Barcodeeingabe", "Code", ref value) == DialogResult.OK)
                {
                    HandleInput(value);
                    MessageBox.Show(string.Format("Code {0} geschrieben", value), Application.ProductName, MessageBoxButtons.OK);
                }
            }
        }

        void scannerPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string rxString = (sender as SerialPort).ReadExisting().Trim();
           
            HandleInput(rxString);
        }

        private void HandleInput(string scannerData)
        {
            Stream str = Properties.Resources.chimes;

            StringBuilder dataString = new StringBuilder(DateTime.Now.ToString("dd/MM/yy;H:mm:ss;"));

            if (Directory.Exists(settings.FolderPath) && fileName.Length > 0)
            {
                if (scale != null)
                    dataString.AppendFormat("{0:0.000}", scale.NetWeight);
                if (scannerData.Length > 0 && settings.Field1Length > 0 && scannerData.Length > settings.Field1Length)
                {
                    dataString.Append(";" + scannerData.Substring(0, settings.Field1Length));
                    scannerData = scannerData.Remove(0, settings.Field1Length);
                }

                if (scannerData.Length > 0 && settings.Field2Length > 0 && scannerData.Length > settings.Field2Length)
                {
                    dataString.Append(";" + scannerData.Substring(0, settings.Field2Length));
                    scannerData = scannerData.Remove(0, settings.Field2Length);
                }

                if (scannerData.Length > 0 && settings.Field3Length > 0 && scannerData.Length > settings.Field3Length)
                {
                    dataString.Append(";" + scannerData.Substring(0, settings.Field3Length));
                    scannerData = scannerData.Remove(0, settings.Field3Length);
                }

                if (scannerData.Length > 0 &&  settings.DismissRest == false)
                    dataString.Append(";"+ scannerData);

                if (!dataString.ToString().Contains("\n"))
                    dataString.AppendLine();

                File.AppendAllText(Path.Combine(settings.FolderPath, fileName), dataString.ToString());
                SoundPlayer snd = new SoundPlayer(str);
                if (settings.PlaySound)
                    snd.Play();

                dataString.Replace(';', ',');

                string selPrinter = PrinterSettings.InstalledPrinters.OfType<string>().FirstOrDefault(d => d == settings.PrinterName);
                if (selPrinter != null)
                    RawPrinterHelper.SendStringToPrinter(selPrinter, dataString.ToString());
            }
        }

        # region the child forms



        private void ShowSettingsForm()
        {
            if (scale != null && scale.IsOpen)
                scale.Close();

            if (scannerPort != null && scannerPort.IsOpen)
                scannerPort.Close();

            if (settingsForm == null)
            {
                settingsForm = new frmSettings(settings);
                settingsForm.Closed += settingsForm_Closed; // avoid reshowing a disposed form
                settingsForm.ShowDialog();
            }
            else { settingsForm.Activate(); }

            OpenPorts();
            if (!Directory.Exists(settings.FolderPath))
                Directory.CreateDirectory(settings.FolderPath);
        }

        private void settingsForm_Closed(object sender, EventArgs e)
        {
            settingsForm = null;
        }

        private void HandleRecording()
        {
            if (running == false)
            {
                if (scale == null)
                    MessageBox.Show("Waage gibt keine Antwort !", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {

                    string value = "Protokoll_" + DateTime.Now.ToString("yyMMddHmmss") + settings.Extension;
                    if (DataEntry.InputBox("Dateinamen eingeben", "Dateinamen:", ref value) == DialogResult.OK)
                    {
                        fileName = value;
                        running = true;
                        notifyIcon.ContextMenuStrip.Items["Action"].Text = "Stop";
                        notifyIcon.ContextMenuStrip.Items["Config"].Enabled = false;
                        notifyIcon.ContextMenuStrip.Items["Manuell"].Enabled = true;
                    }
                }
            }
            else
            {
                running = false;
                notifyIcon.ContextMenuStrip.Items["Action"].Text = "Start";
                notifyIcon.ContextMenuStrip.Items["Config"].Enabled = true;
                notifyIcon.ContextMenuStrip.Items["Manuell"].Enabled = false;
                fileName = "";
            }
        }


        # endregion the child forms

        # region generic code framework

        /// <summary>
        /// When the application context is disposed, dispose things like the notify icon.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (scale != null)
            {
                scale.Close();
            }
            if (scannerPort != null)
            {
                scannerPort.Close();
            }
            CommunicationEngine.Dispose();

            if (disposing && components != null) { components.Dispose(); }
        }

        /// <summary>
        /// When the exit menu item is clicked, make a call to terminate the ApplicationContext.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitItem_Click(object sender, EventArgs e)
        {
            ExitThread();
        }

        /// <summary>
        /// If we are presently showing a form, clean it up.
        /// </summary>
        protected override void ExitThreadCore()
        {
            // before we exit, let forms clean themselves up.
            if (settingsForm != null) { settingsForm.Close(); }
            notifyIcon.Visible = false; // should remove lingering tray icon
            base.ExitThreadCore();
        }

        # endregion generic code framework

    }
}
